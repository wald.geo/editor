#!/bin/bash

SERVER_DOMAIN=inforest.cloud
USER=sh
PROJECT_FOLDER=/home/nodejs/pflanzguide

TIMESTAMP=`date +"%Y%m%d%H%M%S"`
RELEASE_FOLDER=${PROJECT_FOLDER}/releases/${TIMESTAMP}

ssh ${USER}@${SERVER_DOMAIN} << EOF
  mkdir ${RELEASE_FOLDER}
EOF

scp -r $(ls --ignore=node_modules) ${USER}@${SERVER_DOMAIN}:${RELEASE_FOLDER}

ssh ${USER}@${SERVER_DOMAIN} << EOF
  cd ${RELEASE_FOLDER}
  rm ${PROJECT_FOLDER}/current
  ln -s ${RELEASE_FOLDER} ${PROJECT_FOLDER}/current

  cd releases
  ls -lt | tail -n +4 | awk '{print $9}' | xargs rm -r
EOF

echo "Check the access rights for the data folder!"
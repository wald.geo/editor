const   fs = require('fs'),
        watch = require('node-watch'),
        schemaPW = JSON.parse(fs.readFileSync(__dirname + '/../assets/schema_projektionswege.json')),
        schemaWG = JSON.parse(fs.readFileSync(__dirname + '/../assets/schema_waldgesellschaften.json')),
        LUT = JSON.parse(fs.readFileSync(__dirname + '/../assets/lut_waldgesellschaften.json'))
        ;

let     projektionswege = JSON.parse(fs.readFileSync(__dirname + '/../data/projektionswege.json', 'utf-8')),
        waldgesellschaften = JSON.parse(fs.readFileSync(__dirname + '/../data/waldgesellschaften.json', 'utf-8'))
        ;



module.exports = function(app) {

    //API routes
    app.get('/api/schema-pw', function(req, res) {
        res.json(schemaPW);
    });
    
    app.get('/api/schema-pw.json', function(req, res) {
        res.json(schemaPW);
    });
    
    app.get('/api/projektionswege', function(req, res) {
        res.json(projektionswege);
    });
    
    app.get('/api/projektionswege.json', function(req, res) {
        res.json(projektionswege);
    });

    app.get('/api/lut', function(req, res) {
        res.json(LUT);
    });
    
    app.get('/api/lut.json', function(req, res) {
        res.json(LUT);
    });

    app.get('/api/schema-wg', function(req, res) {
        res.json(schemaWG);
    });
    
    app.get('/api/schema-wg.json', function(req, res) {
        res.json(schemaWG);
    });
    
    app.get('/api/waldgesellschaften', function(req, res) {
        res.json(waldgesellschaften);
    });
    
    app.get('/api/waldgesellschaften.json', function(req, res) {
        res.json(waldgesellschaften);
    });

    app.post('/api/editor-pw-update', function(req, res) {
        // console.log(req.body);
        fs.writeFile(__dirname + '/../data/projektionswege.json', JSON.stringify(req.body), 'utf8', function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The user has updated data/projektionswege.json");
            res.send("Server: I received your PW-Data. *Bliblup*");
        }); 
    });
    
    app.post("/api/editor-wg-update", function(req, res) {
        // console.log(req.body);
        fs.writeFile(__dirname + '/../data/waldgesellschaften.json', JSON.stringify(req.body), 'utf8', function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The user has updated data/waldgesellschaften.json");
            res.send("Server: I received your WG-Data. *Bliblup*");
        }); 
    });

    // Watch Json Data Updates
    watch(__dirname + '/../data/waldgesellschaften.json', { recursive: true }, function(evt, name) {
        waldgesellschaften = JSON.parse(fs.readFileSync(__dirname + '/../data/waldgesellschaften.json', 'utf-8'));
        console.log('%s changed.', name);
    });
    
      watch(__dirname + '/../data/projektionswege.json', { recursive: true }, function(evt, name) {
        projektionswege = JSON.parse(fs.readFileSync(__dirname + '/../data/projektionswege.json', 'utf-8'));
        console.log('%s changed.', name);
    });

};

var http = require('http'),
    fs = require('fs'),
    path = require('path'),
    watch = require('node-watch');


module.exports = function(app) {

//Watch .json file changes
    watch(__dirname + '/../assets/projektionswege.json', { recursive: true }, function(evt, name) {
        projektionswege = JSON.parse(fs.readFileSync(__dirname + '/../assets/projektionswege.json', 'utf-8'));
        console.log('%s refreshed.', name);
    });
    
    watch(__dirname + '/../assets/waldgesellschaften.json', { recursive: true }, function(evt, name) {
        waldgesellschaften = JSON.parse(fs.readFileSync(__dirname + '/../assets/waldgesellschaften.json', 'utf-8'));
        console.log('%s refreshed.', name);
    });

};